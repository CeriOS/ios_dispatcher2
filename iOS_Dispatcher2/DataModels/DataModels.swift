//
//  DataModels.swift
//  iOS_Dispatcher2
//
//  Created by Rey Cerio on 2017-10-04.
//  Copyright © 2017 Rey Cerio. All rights reserved.
//

import Foundation

struct DriverLocation{
    var latitude: String?
    var longitude: String?
    var speed: String?
    var date: String?
    var userId: String?
    var truckId: String?
    var status: String?
}

