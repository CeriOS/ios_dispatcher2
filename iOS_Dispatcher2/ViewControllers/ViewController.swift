//
//  ViewController.swift
//  iOS_Dispatcher2
//
//  Created by Rey Cerio on 2017-10-04.
//  Copyright © 2017 Rey Cerio. All rights reserved.
//

import UIKit
import Firebase

class LoginController: UIViewController {
    
    let emailTextField : UITextField = {
        let tf = UITextField()
        tf.placeholder = "Email"
        tf.keyboardType = .emailAddress
        tf.autocapitalizationType = .none
        tf.autocorrectionType = .no
        tf.borderStyle = .roundedRect
        tf.layer.cornerRadius = 6
        tf.layer.masksToBounds = true
        return tf
    }()
    
    let passwordTextField : UITextField = {
        let tf = UITextField()
        tf.placeholder = "Password"
        tf.autocorrectionType = .no
        tf.autocapitalizationType = .none
        tf.isSecureTextEntry = true
        tf.borderStyle = .roundedRect
        tf.layer.cornerRadius = 6
        tf.layer.masksToBounds = true
        return tf
    }()
    
    let loginButton : UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Login", for: .normal)
        button.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        button.layer.cornerRadius = 6
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.gray.cgColor
        button.layer.masksToBounds = true
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        setupViews()
    }

    func setupViews() {
        view.addSubview(emailTextField)
        view.addSubview(passwordTextField)
        view.addSubview(loginButton)
        
        view.addConstraintsWithVisualFormat(format: "H:|-16-[v0]-16-|", views: emailTextField)
        view.addConstraintsWithVisualFormat(format: "H:|-16-[v0]-16-|", views: passwordTextField)
        view.addConstraintsWithVisualFormat(format: "H:|-150-[v0(100)]", views: loginButton)
        
        view.addConstraintsWithVisualFormat(format: "V:|-100-[v0(50)]-10-[v1(50)]-50-[v2(50)]", views: emailTextField, passwordTextField, loginButton)
    }
    
    @objc func handleLogin() {
        
        if emailTextField.text == nil || passwordTextField.text == nil {
            self.createAlert(title: "Empty Fields.", message: "Please make sure both input fields are filled.")
        } else {
            guard let email = emailTextField.text else {return}
            guard let password = passwordTextField.text else {return}
            Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
                if error != nil {
                    self.createAlert(title: "Could not Login.", message: "Please try again")
                    return
                } else {
                    let layout = UICollectionViewFlowLayout()
                    let truckCollectionView = TruckIdCollectionViewController(collectionViewLayout: layout)
                    let navTruckListController = UINavigationController(rootViewController: truckCollectionView)
                    self.present(navTruckListController, animated: true, completion: nil)
                }
            })
            
        }
        
    }
    
    func createAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            //self.dismiss(animated: true, completion: nil)
            return
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

