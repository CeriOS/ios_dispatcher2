//
//  MapViewController.swift
//  iOS_Dispatcher2
//
//  Created by Rey Cerio on 2017-10-04.
//  Copyright © 2017 Rey Cerio. All rights reserved.
//

import UIKit
import MapKit
import Firebase

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    var truckId: String? {
        didSet{
            navigationItem.title = truckId
        }
    }
    var driverLocation = DriverLocation()
    var driverLat = Double()
    var driverLong = Double()
    
    var speedLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = .boldSystemFont(ofSize: 20)
        label.backgroundColor = UIColor.clear
        return label
    }()
    
    let mapView : MKMapView = {
        let map = MKMapView()
        return map
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        fetchDriverLocation()
        setupLocationManager()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Trips", style: .plain, target: self, action: #selector(previousTripsReport))
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(handleBack))
    }
    
    @objc func handleBack() {
        locationManager.stopUpdatingLocation()
        let layout = UICollectionViewFlowLayout()
        let truckListController = TruckIdCollectionViewController(collectionViewLayout: layout)
        let navTruckList = UINavigationController(rootViewController: truckListController)
        self.present(navTruckList, animated: true, completion: nil)
    }
    
    @objc func previousTripsReport() {
        let previousTripsController = PreviousTripsReportController()
        previousTripsController.truckId = self.truckId
        let navPreviousTrips = UINavigationController(rootViewController: previousTripsController)
        self.present(navPreviousTrips, animated: true, completion: nil)
    }
    
    func setupViews() {
        view.addSubview(mapView)
        view.addSubview(speedLabel)
        
        view.addConstraintsWithVisualFormat(format: "H:[v0(150)]-4-|", views: speedLabel)
        view.addConstraintsWithVisualFormat(format: "V:|-50-[v0(50)]", views: speedLabel)
        view.addConstraintsWithVisualFormat(format: "H:|[v0]|", views: mapView)
        view.addConstraintsWithVisualFormat(format: "V:|[v0]|", views: mapView)
    }
    
    func fetchDriverLocation() {
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let truckId = self.truckId else {return}
        let dataRef = Database.database().reference().child("Cer_Driver_Location").child(uid).child(truckId)
//        dataRef.observe(.childAdded, with: { (snapshot) in
//            if !snapshot.exists() {
//                print("Can not find location.")
//                return
//            }
//            let dictionary = snapshot.value as! [String: Any]
//            self.driverLocation.userId = uid
//            self.driverLocation.truckId = self.truckId
//            self.driverLocation.latitude = dictionary["latitude"] as? String
//            self.driverLocation.longitude = dictionary["longitude"] as? String
//            self.driverLocation.speed = dictionary["speed"] as? String
//            self.driverLocation.date = dictionary["date"] as? String
//            self.driverLocation.status = dictionary["status"] as? String
//            self.driverLat = Double(self.driverLocation.latitude!)!
//            self.driverLong = Double(self.driverLocation.longitude!)!
//            guard let speed = self.driverLocation.speed else {
//                self.speedLabel.text = "0.0 km/h"
//                return
//            }
//            
//            self.speedLabel.text = speed
//        }, withCancel: nil)
        
        dataRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if !snapshot.exists() {
                print("Can not find location.")
                return
            }
            let dictionary = snapshot.value as! [String: Any]
            self.driverLocation.userId = uid
            self.driverLocation.truckId = self.truckId
            self.driverLocation.latitude = dictionary["latitude"] as? String
            self.driverLocation.longitude = dictionary["longitude"] as? String
            self.driverLocation.speed = dictionary["speed"] as? String
            self.driverLocation.date = dictionary["date"] as? String
            self.driverLocation.status = dictionary["status"] as? String
            self.driverLat = Double(self.driverLocation.latitude!)!
            self.driverLong = Double(self.driverLocation.longitude!)!
            guard let speed = self.driverLocation.speed else {
                self.speedLabel.text = "0.0 km/h"
                return
            }
            
            self.speedLabel.text = speed
        }, withCancel: nil)
    }
    
    func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
            fetchDriverLocation()
            
            let driverLoc = CLLocationCoordinate2D(latitude: driverLat, longitude: driverLong)
            let region = MKCoordinateRegion(center: driverLoc, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            
            self.mapView.setRegion(region, animated: true)
            self.mapView.removeAnnotations(self.mapView.annotations)
            let annotation = MKPointAnnotation()
            annotation.coordinate = driverLoc
            annotation.title = "Driver Location"
            
            self.mapView.addAnnotation(annotation)
        
    }
    
}
