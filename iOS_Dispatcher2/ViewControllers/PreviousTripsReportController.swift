//
//  PreviousTripsReportController.swift
//  iOS_Dispatcher2
//
//  Created by Rey Cerio on 2017-10-21.
//  Copyright © 2017 Rey Cerio. All rights reserved.
//

import UIKit
import Firebase
import MapKit

class PreviousTripsReportController: UIViewController {

    var truckId: String? {
        didSet{
            navigationItem.title = self.truckId
        }
    }
    
    let dateTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Enter Date: (YYYY-MM-DD)"
        return tf
    }()
    
    let submitButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Submit", for: .normal)
        button.addTarget(self, action: #selector(handleSubmit), for: .touchUpInside)
        button.layer.cornerRadius = 6
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.gray.cgColor
        button.layer.masksToBounds = true
        return button
    }()
    
    let mapView = MKMapView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
        
    }

    @objc func handleSubmit() {
        
    }
    
    func setupViews() {
        view.addSubview(dateTextField)
        view.addSubview(submitButton)
        view.addSubview(mapView)
        
        view.addConstraintsWithVisualFormat(format: "H:|-16-[v0]-16-|", views: dateTextField)
        view.addConstraintsWithVisualFormat(format: "H:|-150-[v0(100)]", views: submitButton)
        view.addConstraintsWithVisualFormat(format: "H:|[v0]|", views: mapView)
        view.addConstraintsWithVisualFormat(format: "V:|-100-[v0(50)]-20-[v1(40)]-10-[v2]|", views: dateTextField, submitButton, mapView)
    }
    
    func fetchLocationForCertainDay() {
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let chosenDate: String = dateTextField.text else {return}
        
        //convert dateString to Date() then convert it to date interval
        //find the dates within the nodes in database.
        //plug them into an array
        
        
        //or
        
        //make another child node in firebase under the saved locations titled with the current dates with no time.
        
    }
    
}






















