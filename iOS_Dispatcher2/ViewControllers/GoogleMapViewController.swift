//
//  GoogleMapViewController.swift
//  iOS_Dispatcher2
//
//  Created by Rey Cerio on 2017-10-06.
//  Copyright © 2017 Rey Cerio. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase

class GoogleMapViewController: UIViewController  {

    var truckId: String? {
        didSet{
            navigationItem.title = truckId
        }
    }
    
    var driverLocation = DriverLocation()
    var driverLat = Double()
    var driverLong = Double()
    
    var speedLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = .boldSystemFont(ofSize: 20)
        label.backgroundColor = UIColor.clear
        return label
    }()
    
    func createMapInstance() -> GMSMapView {
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        let camera = GMSCameraPosition.camera(withLatitude: 0.0, longitude: 0.0, zoom: 17.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        return mapView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(handleBack))
        setupView()
        fetchDriverLocation()
    }
    
    //override func loadView()
    func createGoogleMapMarkers(mapView: GMSMapView) {
        
//        // Create a GMSCameraPosition that tells the map to display the
//        // coordinate -33.86,151.20 at zoom level 6.
//        let camera = GMSCameraPosition.camera(withLatitude: 0.0, longitude: 0.0, zoom: 17.0)
//        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = mapView
        mapView.clear()
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: self.driverLat, longitude: self.driverLong)
        mapView.animate(toLocation: marker.position)
        print(self.driverLat, self.driverLong)
        marker.title = self.driverLocation.truckId
        marker.snippet = "driver location"
        marker.map = mapView
    }
    
    func setupView() {
        view.addSubview(speedLabel)
        
        view.addConstraintsWithVisualFormat(format: "H:[v0(150)]-4-|", views: speedLabel)
        view.addConstraintsWithVisualFormat(format: "V:|-50-[v0(50)]", views: speedLabel)
    }
    
    func fetchDriverLocation() {
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let truckId = self.truckId else {return}
        let dataRef = Database.database().reference().child("Cer_Driver_Location").child(uid).child(truckId)
        
        dataRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if !snapshot.exists() {
                print("Can not find location.")
                return
            }
            let dictionary = snapshot.value as! [String: Any]
            self.driverLocation.userId = uid
            self.driverLocation.truckId = self.truckId
            self.driverLocation.latitude = dictionary["latitude"] as? String
            self.driverLocation.longitude = dictionary["longitude"] as? String
            self.driverLocation.speed = dictionary["speed"] as? String
            self.driverLocation.date = dictionary["date"] as? String
            self.driverLocation.status = dictionary["status"] as? String
            self.driverLat = Double(self.driverLocation.latitude!)!
            self.driverLong = Double(self.driverLocation.longitude!)!
            guard let speed = self.driverLocation.speed else {
                self.speedLabel.text = "0.0 km/h"
                return
            }
            self.speedLabel.text = speed + " km/h"

            DispatchQueue.main.async(execute: {
                //reload the view (mapView)
                self.createGoogleMapMarkers(mapView: self.createMapInstance())
            })
        }, withCancel: nil)
    }
    
    @objc func handleBack() {
        let layout = UICollectionViewFlowLayout()
        let truckListController = TruckIdCollectionViewController(collectionViewLayout: layout)
        let navTruckList = UINavigationController(rootViewController: truckListController)
        self.present(navTruckList, animated: true, completion: nil)
    }
}
