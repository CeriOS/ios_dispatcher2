//
//  TruckIdCollectionViewController.swift
//  iOS_Dispatcher2
//
//  Created by Rey Cerio on 2017-10-04.
//  Copyright © 2017 Rey Cerio. All rights reserved.
//

import UIKit
import Firebase

private let reuseIdentifier = "Cell"

class TruckIdCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var timer = Timer()
    var truckId = [String]()
    var status = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.isScrollEnabled = true
        view.backgroundColor = .white
        collectionView?.backgroundColor = .white
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handleLogout))
        collectionView?.register(TruckListCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        fetchTruckIds()
        
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return truckId.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! TruckListCollectionViewCell
        
        let truckId = self.truckId[indexPath.item]
        cell.truckIdLabel.text = truckId
        activePing(truckId: truckId, cell: cell)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 50)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let mapViewController = MapViewController()
        mapViewController.truckId = self.truckId[indexPath.item]
        let navMapViewController = UINavigationController (rootViewController: mapViewController)
        present(navMapViewController, animated: true, completion: nil)
        
//        let googleMapViewController = GoogleMapViewController()
//        googleMapViewController.truckId = self.truckId[indexPath.item]
//        let navMapViewController = UINavigationController (rootViewController: googleMapViewController)
//        self.present(navMapViewController, animated: true, completion: nil)
        
    }
    
    func fetchTruckIds() {
        guard let uid = Auth.auth().currentUser?.uid else {return}
        let databaseRef = Database.database().reference().child("Cer_Driver_Location").child(uid)
        databaseRef.observe(.childAdded, with: { (snapshot) in

            if !snapshot.exists() {
                print("TruckId not found!")
                return
            }
            let truckId = snapshot.key
            self.truckId.append(truckId)
            print(self.truckId)
            
//            self.reloadInMainThread()
            
            
//            self.timer.invalidate()
//            self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.reloadInMainThread), userInfo: nil, repeats: false)
            DispatchQueue.main.async(execute: {
                self.collectionView?.reloadData()
            })
        }, withCancel: nil)
    }
    
    func reloadInMainThread() {
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
        }
    }
    
    func activePing(truckId: String, cell: TruckListCollectionViewCell) {
        guard let uid = Auth.auth().currentUser?.uid else {fatalError()}
        guard let truckId = cell.truckIdLabel.text else {return}
        let statusRef = Database.database().reference().child("Cer_Driver_Location").child(uid).child(truckId)
        statusRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if !snapshot.exists() {
                print("Cannot find status...")
                return
            }
            let dictionary = snapshot.value as! [String: Any]
            let status = dictionary["status"] as! String
            if (status == "Online") {
                cell.onlineStatusLabel.text = "Online"
                cell.onlineStatusLabel.textColor = self.view.tintColor
            } else {
                cell.onlineStatusLabel.text = "Offline"
                cell.onlineStatusLabel.textColor = .red
            }
            self.timer.invalidate()
//            self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.reloadInMainThread), userInfo: nil, repeats: false)
            self.timer = Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false, block: { (timer) in
                self.reloadInMainThread()
            })
        }, withCancel: nil)
    }

    @objc func handleLogout() {
        do {
            try Auth.auth().signOut()
        } catch {
            self.createAlert(title: "Something went wrong", message: "Wait and try again.")
        }
        let loginController = LoginController()
        self.present(loginController, animated: true, completion: nil)
    }
    
    func createAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            //self.dismiss(animated: true, completion: nil)
            return
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

//let dictionary = snapshot.value as! [String: Any]
//let status = dictionary["status"] as! String

//            let dictionary = snapshot.value as! [String: Any]
//            var timestamp = Float64()
//            timestamp = dictionary["timestamp"] as! Float64
//
//            print(String(describing: timestamp))
//
//            let date = Date()
//            let dateConverted = date.timeIntervalSince1970
//            var statusString = String()
//            if dateConverted - timestamp < 180000 {
//                statusString = "Online"
//                self.status.append(statusString)
//            } else {
//                statusString = "Offline"
//                self.status.append(statusString)
//            }


























