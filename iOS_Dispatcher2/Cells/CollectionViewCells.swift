//
//  CollectionViewCells.swift
//  iOS_Dispatcher2
//
//  Created by Rey Cerio on 2017-10-04.
//  Copyright © 2017 Rey Cerio. All rights reserved.
//

import UIKit

class BaseCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        
    }
}

class TruckListCollectionViewCell: BaseCell {
    
    let truckIdLabel : UILabel = {
        let label = UILabel()
        return label
    }()
    
    let onlineStatusLabel : UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()
    
    override func setupViews() {
        addSubview(truckIdLabel)
        addSubview(onlineStatusLabel)
        
        addConstraintsWithVisualFormat(format: "H:|[v0(300)]-2-[v1]|", views: truckIdLabel, onlineStatusLabel)
        addConstraintsWithVisualFormat(format: "V:|[v0]|", views: truckIdLabel)
        addConstraintsWithVisualFormat(format: "V:|[v0]|", views: onlineStatusLabel)
        
    }
    
//    override func prepareForReuse() {
//        truckIdLabel.text = ""
//        truckIdLabel.textColor = .black
//        onlineStatusLabel.text = ""
//        onlineStatusLabel.textColor = .black
//    }
}
